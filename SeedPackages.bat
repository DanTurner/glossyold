@echo off
SET ROOT=%CD%
SET PATH=%PATH%;%ROOT%\tools\NuGet
cd packages
NuGet.exe install ..\src\Glossy.Application\packages.config
NuGet.exe install ..\src\Glossy.Application.Tests\packages.config
NuGet.exe install ..\src\Glossy.Web\packages.config
NuGet.exe install ..\src\Glossy.Web.Tests\packages.config
NuGet.exe install ..\src\Glossy.Integration.Tests\packages.config
cd %ROOT%
pause