﻿using System.Collections.Generic;
using System.Linq;
using Glossy.Application.Services;
using Glossy.Core;
using Moq;
using NUnit.Framework;
using Raven.Client;

namespace Glossy.Application.Tests.Services
{
    [TestFixture]
    public class RavenGlossaryServiceTests
    {
        #region Setup/Teardown

        [SetUp]
        public void CreateGlossary()
        {
            TestingTermA = new Term("Term A");
            TestingDefinitionA = new Definition("This is term A");
            TestingTermB = new Term("Term B");
            TestingDefinitionB = new Definition("This is term B");

            Glossary = new Glossary(new Dictionary<Term, Definition>
                                        {
                                            {TestingTermB, TestingDefinitionB},
                                            {TestingTermA, TestingDefinitionA}
                                        });

            Document = new GlossaryDocument
                           {
                               Glossary = Glossary
                           };
        }

        #endregion

        private GlossaryDocument Document { get; set; }
        private Glossary Glossary { get; set; }
        private Term TestingTermA { get; set; }
        private Definition TestingDefinitionA { get; set; }

        private Term TestingTermB { get; set; }
        private Definition TestingDefinitionB { get; set; }

        [Test]
        public void can_get_existing_glossary()
        {
            var session = new Mock<IDocumentSession>();
            session.Setup(u => u.Load<GlossaryDocument>(GlossaryDocument.DEFAULT_GLOSSARY)).Returns(Document);

            var service = new RavenGlossaryService(session.Object);

            Glossary result = service.Get();

            Assert.AreSame(Document.Glossary, result);

            session.Verify(u => u.Load<GlossaryDocument>(GlossaryDocument.DEFAULT_GLOSSARY), Times.Once());
        }

        [Test]
        public void can_get_glossary_for_the_first_time()
        {
            var session = new Mock<IDocumentSession>();
            session.Setup(u => u.Load<GlossaryDocument>(GlossaryDocument.DEFAULT_GLOSSARY)).Returns(
                default(GlossaryDocument));

            var service = new RavenGlossaryService(session.Object);

            Glossary result = service.Get();

            Assert.NotNull(result);
            Assert.AreEqual(0, result.Terms().Count());

            session.Verify(u => u.Load<GlossaryDocument>(GlossaryDocument.DEFAULT_GLOSSARY), Times.Once());
        }

        [Test]
        public void can_save_glossary()
        {
            var session = new Mock<IDocumentSession>();

            session.Setup(u => u.Load<GlossaryDocument>(GlossaryDocument.DEFAULT_GLOSSARY)).Returns(Document);
            session.Setup(u => u.Store(Document));

            var service = new RavenGlossaryService(session.Object);

            var newGlossary = new Glossary(new Dictionary<Term, Definition>());

            service.Save(newGlossary);

            Assert.AreEqual(newGlossary, Document.Glossary);

            session.Verify(u => u.Load<GlossaryDocument>(GlossaryDocument.DEFAULT_GLOSSARY), Times.Once());
            session.Verify(u => u.Store(Document), Times.Once());
        }
    }
}