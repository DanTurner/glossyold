﻿using System.Collections.Generic;
using FluentValidation.Results;
using Glossy.Application.Commands;
using Glossy.Application.Commands.Validation;
using Glossy.Application.Services;
using Glossy.Core;
using Moq;
using NUnit.Framework;

namespace Glossy.Application.Tests.Commands.Validation
{
    [TestFixture]
    public class DefineTermCommandValidatorTests
    {
        #region Setup/Teardown

        [SetUp]
        public void CreateGlossary()
        {
            Glossary = new Glossary(new Dictionary<Term, Definition> {{"Testing", "This is a test"}});
        }

        #endregion

        private Glossary Glossary { get; set; }

        [Test]
        public void can_define_new_term()
        {
            var command = new DefineTermCommand("New term", "This is a new term");

            var service = new Mock<IGlossaryService>();
            service.Setup(u => u.Get()).Returns(Glossary);
            var validator = new DefineTermCommandValidator(service.Object);

            ValidationResult results = validator.Validate(command);

            service.Verify(u => u.Get(), Times.Once());
            Assert.IsTrue(results.IsValid);
        }

        [Test]
        public void cannot_define_empty_term()
        {
            var command = new DefineTermCommand("", "This is an empty definition");

            var service = new Mock<IGlossaryService>();
            service.Setup(u => u.Get()).Returns(Glossary);
            var validator = new DefineTermCommandValidator(service.Object);

            ValidationResult results = validator.Validate(command);

            service.Verify(u => u.Get(), Times.Never());
            Assert.IsFalse(results.IsValid);
        }

        [Test]
        public void cannot_define_existing_term()
        {
            var command = new DefineTermCommand("Testing", "This is an existing definition");

            var service = new Mock<IGlossaryService>();
            service.Setup(u => u.Get()).Returns(Glossary);
            var validator = new DefineTermCommandValidator(service.Object);

            ValidationResult results = validator.Validate(command);

            service.Verify(u => u.Get(), Times.Once());
            Assert.IsFalse(results.IsValid);
        }

        [Test]
        public void cannot_define_with_empty_definition()
        {
            var command = new DefineTermCommand("Empty term", "");

            var service = new Mock<IGlossaryService>();
            service.Setup(u => u.Get()).Returns(Glossary);
            var validator = new DefineTermCommandValidator(service.Object);

            ValidationResult results = validator.Validate(command);

            service.Verify(u => u.Get(), Times.Never());
            Assert.IsFalse(results.IsValid);
        }
    }
}