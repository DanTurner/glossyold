﻿using System.Collections.Generic;
using FluentValidation.Results;
using Glossy.Application.Commands;
using Glossy.Application.Commands.Validation;
using Glossy.Application.Services;
using Glossy.Core;
using Moq;
using NUnit.Framework;

namespace Glossy.Application.Tests.Commands.Validation
{
    [TestFixture]
    public class ReRedefineTermCommandValidatorTests
    {
        #region Setup/Teardown

        [SetUp]
        public void CreateGlossary()
        {
            TestingTerm = new Term("Testing");
            TestingDefinition = new Definition("This is a test");
            TestingAnotherTerm = new Term("Testing Another");
            TestingAnotherDefinition = new Definition("This is another test");

            Glossary = new Glossary(new Dictionary<Term, Definition>
                                        {
                                            {TestingTerm, TestingDefinition},
                                            {TestingAnotherTerm, TestingAnotherDefinition}
                                        });
        }

        #endregion

        private Glossary Glossary { get; set; }

        private Term TestingTerm { get; set; }
        private Definition TestingDefinition { get; set; }

        private Term TestingAnotherTerm { get; set; }
        private Definition TestingAnotherDefinition { get; set; }

        [Test]
        public void can_redefine_existing_term()
        {
            var command = new RedefineTermCommand(TestingTerm.Value, TestingDefinition.Value);

            command.NewDefinition = "This is a new definition";

            var service = new Mock<IGlossaryService>();
            service.Setup(u => u.Get()).Returns(Glossary);
            var validator = new RedefineTermCommandValidator(service.Object);

            ValidationResult results = validator.Validate(command);

            service.Verify(u => u.Get(), Times.Never());
            Assert.IsTrue(results.IsValid);
        }

        [Test]
        public void can_redefine_with_new_term()
        {
            var command = new RedefineTermCommand(TestingTerm.Value, TestingDefinition.Value);

            command.NewTerm = "New Term";

            var service = new Mock<IGlossaryService>();
            service.Setup(u => u.Get()).Returns(Glossary);
            var validator = new RedefineTermCommandValidator(service.Object);

            ValidationResult results = validator.Validate(command);

            service.Verify(u => u.Get(), Times.Once());
            Assert.IsTrue(results.IsValid);
        }

        [Test]
        public void cannot_redefine_to_another_existing_new_term()
        {
            var command = new RedefineTermCommand(TestingTerm.Value, TestingDefinition.Value);

            command.NewTerm = TestingAnotherTerm.Value;
            command.NewDefinition = "This is another existing term";

            var service = new Mock<IGlossaryService>();
            service.Setup(u => u.Get()).Returns(Glossary);
            var validator = new RedefineTermCommandValidator(service.Object);

            ValidationResult results = validator.Validate(command);

            service.Verify(u => u.Get(), Times.Once());
            Assert.IsFalse(results.IsValid);
        }

        [Test]
        public void cannot_redefine_with_empty_definition()
        {
            var command = new RedefineTermCommand(TestingTerm.Value, TestingDefinition.Value);

            command.NewDefinition = "";

            var service = new Mock<IGlossaryService>();
            service.Setup(u => u.Get()).Returns(Glossary);
            var validator = new RedefineTermCommandValidator(service.Object);

            ValidationResult results = validator.Validate(command);

            service.Verify(u => u.Get(), Times.Never());
            Assert.IsFalse(results.IsValid);
        }

        [Test]
        public void cannot_redefine_with_empty_new_term()
        {
            var command = new RedefineTermCommand(TestingTerm.Value, TestingDefinition.Value);

            command.NewTerm = "";

            var service = new Mock<IGlossaryService>();
            service.Setup(u => u.Get()).Returns(Glossary);
            var validator = new RedefineTermCommandValidator(service.Object);

            ValidationResult results = validator.Validate(command);

            service.Verify(u => u.Get(), Times.Never());
            Assert.IsFalse(results.IsValid);
        }
    }
}