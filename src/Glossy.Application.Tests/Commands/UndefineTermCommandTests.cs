﻿using Glossy.Application.Commands;
using NUnit.Framework;

namespace Glossy.Application.Tests.Commands
{
    [TestFixture]
    public class UndefineTermCommandTests
    {
        [Test]
        public void can_create_with_default_ctor()
        {
            var command = new UndefineTermCommand();

            Assert.IsNull(command.Term);
        }

        [Test]
        public void can_create_with_initialising_ctor()
        {
            string term = "Some Term";

            var command = new UndefineTermCommand(term);

            Assert.AreEqual(term, command.Term);
        }

        [Test]
        public void can_set_term_with_null()
        {
            var command = new UndefineTermCommand();

            command.Term = null;

            Assert.IsNull(command.Term);
        }

        [Test]
        public void can_set_term_with_value()
        {
            string term = "Some Term";

            var command = new UndefineTermCommand();

            command.Term = term;

            Assert.AreEqual(term, command.Term);
        }

        [Test]
        public void term_property_is_null_for_whitespace()
        {
            string term = "      ";

            var command = new UndefineTermCommand();

            command.Term = term;

            Assert.IsNull(command.Term);
        }

        [Test]
        public void term_property_trims_non_empty_value()
        {
            string term = "    Some Term   ";
            string trimmedTerm = "Some Term";

            var command = new UndefineTermCommand();

            command.Term = term;

            Assert.AreEqual(trimmedTerm, command.Term);
        }
    }
}