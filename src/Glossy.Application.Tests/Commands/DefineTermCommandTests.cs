﻿using Glossy.Application.Commands;
using NUnit.Framework;

namespace Glossy.Application.Tests.Commands
{
    [TestFixture]
    public class DefineTermCommandTests
    {
        [Test]
        public void definition_property_does_trim_value()
        {
            string definition = "    Some Definition   ";
            string trimmedDefinition = "Some Definition";

            var command = new DefineTermCommand();

            command.Definition = definition;

            Assert.AreEqual(trimmedDefinition, command.Definition);
        }

        [Test]
        public void term_property_does_trim_value()
        {
            string term = "    Some Term   ";
            string trimmedTerm = "Some Term";

            var command = new DefineTermCommand();

            command.Term = term;

            Assert.AreEqual(trimmedTerm, command.Term);
        }
    }
}