﻿using System;
using System.Collections.Generic;
using Glossy.Application.Commands;
using Glossy.Application.Commands.Handlers;
using Glossy.Application.Services;
using Glossy.Core;
using Moq;
using NUnit.Framework;
using Glossy.Application.Exceptions;

namespace Glossy.Application.Tests.Commands.Handlers
{
    [TestFixture]
    public class UndefineTermCommandHandlerTests
    {
        #region Setup/Teardown

        [SetUp]
        public void CreateGlossary()
        {
            TestingTerm = new Term("Testing");
            TestingDefinition = new Definition("This is a test");

            Glossary = new Glossary(new Dictionary<Term, Definition>
                                        {
                                            {TestingTerm, TestingDefinition}
                                        });
        }

        #endregion

        private Glossary Glossary { get; set; }
        private Term TestingTerm { get; set; }
        private Definition TestingDefinition { get; set; }

        [Test]
        public void can_undefine_term()
        {
            var command = new UndefineTermCommand(TestingTerm);

            var service = new Mock<IGlossaryService>();
            service.Setup(u => u.Get()).Returns(Glossary);
            service.Setup(u => u.Save(Glossary));

            var handler = new UndefineTermCommandHandler(service.Object);
            handler.Execute(command);

            Assert.IsFalse(Glossary.IsDefined(TestingTerm), "Term is still defined");

            service.Verify(u => u.Get(), Times.Once());
            service.Verify(u => u.Save(Glossary), Times.Once());
        }

        [Test]
        public void undefine_unknown_term_throws_not_found()
        {
            var command = new UndefineTermCommand(Guid.NewGuid().ToString());

            var service = new Mock<IGlossaryService>();
            service.Setup(u => u.Get()).Returns(Glossary);
            service.Setup(u => u.Save(Glossary));

            var handler = new UndefineTermCommandHandler(service.Object);
            Assert.Throws<NotFoundException>(() => handler.Execute(command));

            service.Verify(u => u.Get(), Times.Once());
            service.Verify(u => u.Save(Glossary), Times.Never());
        }
    }
}