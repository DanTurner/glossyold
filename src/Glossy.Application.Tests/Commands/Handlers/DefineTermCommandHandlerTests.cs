﻿using System.Collections.Generic;
using Glossy.Application.Commands;
using Glossy.Application.Commands.Handlers;
using Glossy.Application.Services;
using Glossy.Core;
using Moq;
using NUnit.Framework;

namespace Glossy.Application.Tests.Commands.Handlers
{
    [TestFixture]
    public class DefineTermCommandHandlerTests
    {
        #region Setup/Teardown

        [SetUp]
        public void CreateGlossary()
        {
            TestingTerm = new Term("Testing");
            TestingDefinition = new Definition("This is a test");
            TestingAnotherTerm = new Term("Testing Another");
            TestingAnotherDefinition = new Definition("This is another test");

            Glossary = new Glossary(new Dictionary<Term, Definition>
                                        {
                                            {TestingTerm, TestingDefinition},
                                            {TestingAnotherTerm, TestingAnotherDefinition}
                                        });
        }

        #endregion

        private Glossary Glossary { get; set; }
        private Term TestingTerm { get; set; }
        private Definition TestingDefinition { get; set; }

        private Term TestingAnotherTerm { get; set; }
        private Definition TestingAnotherDefinition { get; set; }

        [Test]
        public void can_define_term()
        {
            var command = new DefineTermCommand("New Term", "This is a new term");

            var service = new Mock<IGlossaryService>();
            service.Setup(u => u.Get()).Returns(Glossary);
            service.Setup(u => u.Save(Glossary));

            var handler = new DefineTermCommandHandler(service.Object);
            handler.Execute(command);

            var term = new Term(command.Term);
            var definition = new Definition(command.Definition);

            Definition result = Glossary.Lookup(term);

            Assert.AreEqual(definition, result);

            service.Verify(u => u.Get(), Times.Once());
            service.Verify(u => u.Save(Glossary), Times.Once());
        }
    }
}