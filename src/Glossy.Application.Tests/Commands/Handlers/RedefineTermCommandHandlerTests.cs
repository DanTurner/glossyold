﻿using System;
using System.Collections.Generic;
using Glossy.Application.Commands;
using Glossy.Application.Commands.Handlers;
using Glossy.Application.Services;
using Glossy.Core;
using Moq;
using NUnit.Framework;
using Glossy.Application.Exceptions;

namespace Glossy.Application.Tests.Commands.Handlers
{
    [TestFixture]
    public class RedefineTermCommandHandlerTests
    {
        #region Setup/Teardown

        [SetUp]
        public void CreateGlossary()
        {
            TestingTerm = new Term("Testing");
            TestingDefinition = new Definition("This is a test");
            TestingAnotherTerm = new Term("Testing Another");
            TestingAnotherDefinition = new Definition("This is another test");

            Glossary = new Glossary(new Dictionary<Term, Definition>
                                        {
                                            {TestingTerm, TestingDefinition},
                                            {TestingAnotherTerm, TestingAnotherDefinition}
                                        });
        }

        #endregion

        private Glossary Glossary { get; set; }
        private Term TestingTerm { get; set; }
        private Definition TestingDefinition { get; set; }

        private Term TestingAnotherTerm { get; set; }
        private Definition TestingAnotherDefinition { get; set; }

        [Test]
        public void can_redefine_term()
        {
            var command = new RedefineTermCommand(TestingTerm.Value, TestingDefinition.Value);
            command.NewTerm = "Redefined Term";
            command.NewDefinition = "This is a redefined term";

            var service = new Mock<IGlossaryService>();
            service.Setup(u => u.Get()).Returns(Glossary);
            service.Setup(u => u.Save(Glossary));

            var handler = new RedefineTermCommandHandler(service.Object);
            handler.Execute(command);

            var term = new Term(command.Term);
            var newTerm = new Term(command.NewTerm);
            var newDefinition = new Definition(command.NewDefinition);

            Assert.IsFalse(Glossary.IsDefined(term), "Term is still defined");

            Definition result = Glossary.Lookup(newTerm);

            Assert.AreEqual(newDefinition, result);

            service.Verify(u => u.Get(), Times.Once());
            service.Verify(u => u.Save(Glossary), Times.Once());
        }

        [Test]
        public void redefine_unknown_term_throws_not_found()
        {
            var command = new RedefineTermCommand(Guid.NewGuid().ToString(), TestingDefinition.Value);
            command.NewTerm = "Redefined Term";
            command.NewDefinition = "This is a redefined term";

            var service = new Mock<IGlossaryService>();
            service.Setup(u => u.Get()).Returns(Glossary);
            service.Setup(u => u.Save(Glossary));

            var handler = new RedefineTermCommandHandler(service.Object);
            Assert.Throws<NotFoundException>(() => handler.Execute(command));

            service.Verify(u => u.Get(), Times.Once());
            service.Verify(u => u.Save(Glossary), Times.Never());
        }
    }
}