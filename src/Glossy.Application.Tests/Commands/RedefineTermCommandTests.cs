﻿using Glossy.Application.Commands;
using NUnit.Framework;

namespace Glossy.Application.Tests.Commands
{
    [TestFixture]
    public class RedefineTermCommandTests
    {
        [Test]
        public void new_definition_property_does_trim_value()
        {
            string newDefinition = "    Some Definition   ";
            string trimmedNewDefinition = "Some Definition";

            var command = new RedefineTermCommand();

            command.NewDefinition = newDefinition;

            Assert.AreEqual(trimmedNewDefinition, command.NewDefinition);
        }

        [Test]
        public void new_term_property_does_trim_value()
        {
            string newTerm = "    Some Term   ";
            string trimmedNewTerm = "Some Term";

            var command = new RedefineTermCommand();

            command.NewTerm = newTerm;

            Assert.AreEqual(trimmedNewTerm, command.NewTerm);
        }
    }
}