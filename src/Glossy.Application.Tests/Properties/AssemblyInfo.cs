﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Glossy.Application.Tests")]
[assembly: AssemblyDescription("These are unit tests for Glossy.Application.")]
[assembly: ComVisible(false)]
[assembly: Guid("cf02ed56-4f4e-4d16-8056-bd9513abab30")]

// Version information in SolutionInfo.cs