﻿using Glossy.Application.Models;
using NUnit.Framework;

namespace Glossy.Application.Tests.Models
{
    [TestFixture]
    public class GlossaryModelTests
    {
        [Test]
        public void entries_are_sorted_alphabetically()
        {
            var model = new GlossaryModel(new[]
                                              {
                                                  new GlossaryModel.Entry("Term Z", "Definition Z"),
                                                  new GlossaryModel.Entry("Term A", "Definition A"),
                                                  new GlossaryModel.Entry("Term B", "Definition B")
                                              });

            Assert.AreEqual("Term A", model.Entries[0].Term);
            Assert.AreEqual("Term B", model.Entries[1].Term);
            Assert.AreEqual("Term Z", model.Entries[2].Term);
        }
    }
}