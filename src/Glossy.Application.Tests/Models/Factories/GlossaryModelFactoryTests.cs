﻿using System.Collections.Generic;
using System.Linq;
using Glossy.Application.Models;
using Glossy.Application.Models.Factories;
using Glossy.Application.Services;
using Glossy.Core;
using Moq;
using NUnit.Framework;

namespace Glossy.Application.Tests.Models.Factories
{
    [TestFixture]
    public class GlossaryModelFactoryTests
    {
        #region Setup/Teardown

        [SetUp]
        public void CreateGlossary()
        {
            TestingTermA = new Term("Term A");
            TestingDefinitionA = new Definition("This is term A");
            TestingTermB = new Term("Term B");
            TestingDefinitionB = new Definition("This is term B");

            Glossary = new Glossary(new Dictionary<Term, Definition>
                                        {
                                            {TestingTermB, TestingDefinitionB},
                                            {TestingTermA, TestingDefinitionA}
                                        });
        }

        #endregion

        private Glossary Glossary { get; set; }
        private Term TestingTermA { get; set; }
        private Definition TestingDefinitionA { get; set; }

        private Term TestingTermB { get; set; }
        private Definition TestingDefinitionB { get; set; }

        [Test]
        public void can_create_model_from_empty_glossary()
        {
            var service = new Mock<IGlossaryService>();
            service.Setup(u => u.Get()).Returns(new Glossary(new Dictionary<Term, Definition>()));

            var handler = new GlossaryModelFactory(service.Object);

            GlossaryModel result = handler.Create();

            Assert.AreEqual(0, result.Entries.Count);
        }

        [Test]
        public void can_create_model_from_glossary()
        {
            var service = new Mock<IGlossaryService>();
            service.Setup(u => u.Get()).Returns(Glossary);

            var handler = new GlossaryModelFactory(service.Object);

            GlossaryModel result = handler.Create();

            Assert.AreEqual(2, result.Entries.Count);
            Assert.AreEqual(1,
                            result.Entries.Count(
                                x => x.Term == TestingTermA.Value && x.Definition == TestingDefinitionA.Value));
            Assert.AreEqual(1,
                            result.Entries.Count(
                                x => x.Term == TestingTermB.Value && x.Definition == TestingDefinitionB.Value));
        }
    }
}