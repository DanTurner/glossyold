﻿using System.Collections.Generic;
using Glossy.Application.Models;
using Glossy.Application.Models.Factories;
using Glossy.Application.Services;
using Glossy.Core;
using Moq;
using NUnit.Framework;
using System;
using Glossy.Application.Exceptions;

namespace Glossy.Application.Tests.Models.Factories
{
    [TestFixture]
    public class RedefineTermModelFactoryTests
    {
        #region Setup/Teardown

        [SetUp]
        public void CreateGlossary()
        {
            TestingTermA = new Term("Term A");
            TestingDefinitionA = new Definition("This is term A");
            TestingTermB = new Term("Term B");
            TestingDefinitionB = new Definition("This is term B");

            Glossary = new Glossary(new Dictionary<Term, Definition>
                                        {
                                            {TestingTermB, TestingDefinitionB},
                                            {TestingTermA, TestingDefinitionA}
                                        });
        }

        #endregion

        private Glossary Glossary { get; set; }
        private Term TestingTermA { get; set; }
        private Definition TestingDefinitionA { get; set; }

        private Term TestingTermB { get; set; }
        private Definition TestingDefinitionB { get; set; }

        [Test]
        public void can_create_model_from_glossary()
        {
            var service = new Mock<IGlossaryService>();
            service.Setup(u => u.Get()).Returns(Glossary);

            var handler = new RedefineTermModelFactory(service.Object);

            var input = new RedefineTermQuery {Term = TestingTermA};
            RedefineTermModel result = handler.Create(input);

            Assert.NotNull(result.Command);
            Assert.AreEqual(TestingTermA.Value, result.Command.Term);
            Assert.AreEqual(TestingTermA.Value, result.Command.NewTerm);
            Assert.AreEqual(TestingDefinitionA.Value, result.Command.NewDefinition);
        }

        [Test]
        public void create_redefine_model_for_non_existing_term_throws_notfound()
        {
            var service = new Mock<IGlossaryService>();
            service.Setup(u => u.Get()).Returns(Glossary);

            var handler = new RedefineTermModelFactory(service.Object);

            var input = new RedefineTermQuery { Term = Guid.NewGuid().ToString() };
            Assert.Throws<NotFoundException>(() => handler.Create(input));
        }
    }
}