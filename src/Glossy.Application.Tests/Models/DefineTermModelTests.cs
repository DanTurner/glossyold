﻿using Glossy.Application.Models;
using NUnit.Framework;

namespace Glossy.Application.Tests.Models
{
    [TestFixture]
    public class DefineTermModelTests
    {
        [Test]
        public void default_ctor_creates_empty_command()
        {
            var model = new DefineTermModel();

            Assert.NotNull(model.Command);
        }
    }
}