﻿using Glossy.Application.Models;
using NUnit.Framework;

namespace Glossy.Application.Tests.Models
{
    [TestFixture]
    public class RedefineTermModelTests
    {
        [Test]
        public void can_create_with_initialising_ctor()
        {
            string term = "Term A";
            string definition = "This is term A";
            var model = new RedefineTermModel(term, definition);

            Assert.NotNull(model.Command);
            Assert.AreEqual(term, model.Command.Term);
            Assert.AreEqual(term, model.Command.NewTerm);
            Assert.AreEqual(definition, model.Command.NewDefinition);
        }
    }
}