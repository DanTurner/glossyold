﻿using Autofac;
using Glossy.Application.Infrastructure.Autofac;
using Glossy.Application.Models;
using Glossy.Application.Models.Factories;
using Moq;
using NUnit.Framework;
using Glossy.Application.Infrastructure;

namespace Glossy.Application.Tests.Infrastructure.Autofac
{
    [TestFixture]
    public class AutofacQueryBusTests
    {
        [Test]
        public void can_create_model_with_query_input()
        {
            var input = new RedefineTermQuery {Term = "Some term"};
            var model = new RedefineTermModel(input.Term, "Some definition");

            var handler = new Mock<IModelFactory<RedefineTermQuery, RedefineTermModel>>();
            handler.Setup(u => u.Create(input)).Returns(model);

            var builder = new ContainerBuilder();
            builder.RegisterInstance(handler.Object).As<IModelFactory<RedefineTermQuery, RedefineTermModel>>();
            IContainer container = builder.Build();

            var queryBus = new AutofacModelBuilder(container);

            RedefineTermModel result = queryBus.Create<RedefineTermQuery, RedefineTermModel>(input);

            Assert.AreSame(model, result);

            handler.Verify(u => u.Create(input), Times.Once());
        }

        [Test]
        public void can_create_model_without_query_input()
        {
            var model = new GlossaryModel(new[]
                                              {
                                                  new GlossaryModel.Entry("Term A", "Definition A"),
                                                  new GlossaryModel.Entry("Term B", "Definition B")
                                              });

            var handler = new Mock<IModelFactory<GlossaryModel>>();
            handler.Setup(u => u.Create()).Returns(model);

            var builder = new ContainerBuilder();
            builder.RegisterInstance(handler.Object).As<IModelFactory<GlossaryModel>>();
            IContainer container = builder.Build();

            var queryBus = new AutofacModelBuilder(container);

            var result = queryBus.Create<GlossaryModel>();

            Assert.AreSame(model, result);

            handler.Verify(u => u.Create(), Times.Once());
        }
    }
}