﻿using System;
using Autofac;
using Glossy.Application.Commands;
using Glossy.Application.Commands.Handlers;
using Glossy.Application.Infrastructure;
using Glossy.Application.Infrastructure.Autofac;
using Moq;
using NUnit.Framework;
using Glossy.Application.Exceptions;

namespace Glossy.Application.Tests.Infrastructure.Autofac
{
    [TestFixture]
    public class AutofacCommandBusTests
    {
        [Test]
        public void does_commit_on_success()
        {
            var command = new DefineTermCommand {Term = "Some term", Definition = "Some definition"};

            var handler = new Mock<ICommandHandler<DefineTermCommand>>();
            handler.Setup(u => u.Execute(command));

            var uow = new Mock<IUnitOfWork>();
            uow.Setup(u => u.Commit());

            var builder = new ContainerBuilder();
            builder.RegisterInstance(handler.Object).As<ICommandHandler<DefineTermCommand>>();
            builder.RegisterInstance(uow.Object).As<IUnitOfWork>();
            IContainer container = builder.Build();

            var commandBus = new AutofacCommandBus(container);

            commandBus.Execute(command);

            handler.Verify(u => u.Execute(command), Times.Once());
            uow.Verify(u => u.Commit(), Times.Once());
        }

        [Test]
        public void does_not_commit_on_exception()
        {
            var command = new DefineTermCommand {Term = "Some term", Definition = "Some definition"};

            var handler = new Mock<ICommandHandler<DefineTermCommand>>();
            handler.Setup(u => u.Execute(command)).Throws<InvalidOperationException>();

            var uow = new Mock<IUnitOfWork>();
            uow.Setup(u => u.Commit());

            var builder = new ContainerBuilder();
            builder.RegisterInstance(handler.Object).As<ICommandHandler<DefineTermCommand>>();
            builder.RegisterInstance(uow.Object).As<IUnitOfWork>();
            IContainer container = builder.Build();

            var commandBus = new AutofacCommandBus(container);

            Assert.Throws<InvalidOperationException>(() => commandBus.Execute(command));

            handler.Verify(u => u.Execute(command), Times.Once());
            uow.Verify(u => u.Commit(), Times.Never());
        }
    }
}