﻿using System;
using Glossy.Application.Infrastructure.RavenDB;
using Moq;
using NUnit.Framework;
using Raven.Client;

namespace Glossy.Application.Tests.Infrastructure.RavenDB
{
    [TestFixture]
    public class RavenUnitOfWorkTests
    {
        [Test]
        public void cannot_commit_same_instance_more_than_once()
        {
            var session = new Mock<IDocumentSession>();
            session.Setup(u => u.SaveChanges());

            var store = new Mock<IDocumentStore>();
            store.Setup(u => u.OpenSession()).Returns(session.Object);

            var uow = new RavenUnitOfWork(store.Object);
            uow.Commit();

            Assert.Throws<InvalidOperationException>(() => uow.Commit());

            session.Verify(u => u.SaveChanges(), Times.Once());
        }

        [Test]
        public void does_open_session_upon_creation()
        {
            var session = new Mock<IDocumentSession>();
            session.Setup(u => u.SaveChanges());

            var store = new Mock<IDocumentStore>();
            store.Setup(u => u.OpenSession()).Returns(session.Object);

            var uow = new RavenUnitOfWork(store.Object);

            Assert.AreEqual(session.Object, uow.Session);
        }

        [Test]
        public void does_save_chanes_on_commit()
        {
            var session = new Mock<IDocumentSession>();
            session.Setup(u => u.SaveChanges());

            var store = new Mock<IDocumentStore>();
            store.Setup(u => u.OpenSession()).Returns(session.Object);

            var uow = new RavenUnitOfWork(store.Object);
            uow.Commit();

            session.Verify(u => u.SaveChanges(), Times.Once());
        }
    }
}