﻿using NUnit.Framework;

namespace Glossy.Application.Tests.Extensions
{
    [TestFixture]
    public class StringExtensionsTests
    {
        [Test]
        public void null_safe_trim_with_empty_string_is_null()
        {
            string value = "";

            string result = value.NullSafeTrim();

            Assert.IsNull(result);
        }

        [Test]
        public void null_safe_trim_with_null_value_is_null()
        {
            string value = null;

            string result = value.NullSafeTrim();

            Assert.IsNull(result);
        }

        [Test]
        public void null_safe_trim_with_value_is_trimmed()
        {
            string value = "   This is a test  ";

            string result = value.NullSafeTrim();

            Assert.AreEqual("This is a test", result);
        }

        [Test]
        public void null_safe_trim_with_whitespace_is_null()
        {
            string value = "        ";

            string result = value.NullSafeTrim();

            Assert.IsNull(result);
        }
    }
}