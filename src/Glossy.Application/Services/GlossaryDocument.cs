﻿using System;
using System.Collections.Generic;
using Glossy.Core;

namespace Glossy.Application.Services
{
    public class GlossaryDocument
    {
        public const string DEFAULT_GLOSSARY = "glossary/default";

        private Glossary _glossary;

        public GlossaryDocument()
        {
            _glossary = new Glossary(new Dictionary<Term, Definition>());
        }

        public string Id
        {
            get { return DEFAULT_GLOSSARY; }
        }

        public Glossary Glossary
        {
            get { return _glossary; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException();
                }
                _glossary = value;
            }
        }

        public override bool Equals(object obj)
        {
            var other = obj as GlossaryDocument;

            return (other != null && other.Id.Equals(Id));
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}