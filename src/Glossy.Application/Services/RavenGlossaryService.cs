﻿using Glossy.Core;
using Raven.Client;

namespace Glossy.Application.Services
{
    public class RavenGlossaryService : IGlossaryService
    {
        private readonly IDocumentSession _session;

        public RavenGlossaryService(IDocumentSession session)
        {
            _session = session;
        }

        #region IGlossaryService Members

        public Glossary Get()
        {
            GlossaryDocument document = LoadDocument();

            return document.Glossary;
        }

        public void Save(Glossary glossary)
        {
            GlossaryDocument document = LoadDocument();

            document.Glossary = glossary;

            _session.Store(document);
        }

        #endregion

        private GlossaryDocument LoadDocument()
        {
            return _session.Load<GlossaryDocument>(GlossaryDocument.DEFAULT_GLOSSARY)
                   ?? new GlossaryDocument();
        }
    }
}