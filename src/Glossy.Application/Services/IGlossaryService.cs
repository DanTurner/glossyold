﻿using Glossy.Core;

namespace Glossy.Application.Services
{
    public interface IGlossaryService
    {
        Glossary Get();
        void Save(Glossary glossary);
    }
}