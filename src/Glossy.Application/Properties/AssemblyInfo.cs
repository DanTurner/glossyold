﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Glossy.Application")]
[assembly: AssemblyDescription("This is the core component of glossy that encapsulates the domain model.")]
[assembly: ComVisible(false)]
[assembly: Guid("542d4ebd-97c3-44ef-bedf-4515808684b5")]

// Version information in SolutionInfo.cs