﻿using System;

namespace Glossy.Application
{
    public static class StringExtensions
    {
        public static string NullSafeTrim(this string value)
        {
            if (String.IsNullOrWhiteSpace(value))
            {
                return null;
            }
            return value.Trim();
        }
    }
}