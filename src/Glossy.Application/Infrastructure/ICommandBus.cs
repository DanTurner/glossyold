﻿namespace Glossy.Application.Infrastructure
{
    public interface ICommandBus
    {
        void Execute<TCommand>(TCommand command);
    }
}