﻿namespace Glossy.Application.Infrastructure
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}