﻿using Glossy.Application.Services;
using Glossy.Core;
using System;
using Glossy.Application.Exceptions;

namespace Glossy.Application.Commands.Handlers
{
    public class RedefineTermCommandHandler : ICommandHandler<RedefineTermCommand>
    {
        private readonly IGlossaryService _service;

        public RedefineTermCommandHandler(IGlossaryService service)
        {
            _service = service;
        }

        #region ICommandHandler<RedefineTermCommand> Members

        public void Execute(RedefineTermCommand command)
        {
            Glossary glossary = _service.Get();

            Term term = command.Term;
            
            if(!glossary.IsDefined(term))
                throw new NotFoundException("Term not found");

            glossary.Undefine(term);
            glossary.Define(command.NewTerm, command.NewDefinition);

            _service.Save(glossary);
        }

        #endregion
    }
}