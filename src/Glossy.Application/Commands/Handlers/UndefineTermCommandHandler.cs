﻿using Glossy.Application.Services;
using Glossy.Core;
using Glossy.Application.Exceptions;

namespace Glossy.Application.Commands.Handlers
{
    public class UndefineTermCommandHandler : ICommandHandler<UndefineTermCommand>
    {
        private readonly IGlossaryService _service;

        public UndefineTermCommandHandler(IGlossaryService service)
        {
            _service = service;
        }

        #region ICommandHandler<UndefineTermCommand> Members

        public void Execute(UndefineTermCommand command)
        {
            Glossary glossary = _service.Get();

            var term = command.Term;
            if (!glossary.IsDefined(term))
                throw new NotFoundException("Term not found");

            glossary.Undefine(term);

            _service.Save(glossary);
        }

        #endregion
    }
}