﻿using Glossy.Application.Services;
using Glossy.Core;

namespace Glossy.Application.Commands.Handlers
{
    public class DefineTermCommandHandler : ICommandHandler<DefineTermCommand>
    {
        private readonly IGlossaryService _service;

        public DefineTermCommandHandler(IGlossaryService service)
        {
            _service = service;
        }

        #region ICommandHandler<DefineTermCommand> Members

        public void Execute(DefineTermCommand command)
        {
            Glossary glossary = _service.Get();

            glossary.Define(new Term(command.Term), new Definition(command.Definition));

            _service.Save(glossary);
        }

        #endregion
    }
}