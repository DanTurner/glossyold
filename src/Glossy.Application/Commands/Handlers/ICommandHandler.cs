﻿namespace Glossy.Application.Commands.Handlers
{
    public interface ICommandHandler<TCommand>
    {
        void Execute(TCommand command);
    }
}