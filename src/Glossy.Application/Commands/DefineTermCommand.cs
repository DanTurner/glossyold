﻿namespace Glossy.Application.Commands
{
    public class DefineTermCommand
    {
        private string _definition;
        private string _term;

        public DefineTermCommand()
        {
        }

        public DefineTermCommand(string term, string defintion)
        {
            Term = term;
            Definition = defintion;
        }

        public string Term
        {
            get { return _term; }
            set { _term = value.NullSafeTrim(); }
        }

        public string Definition
        {
            get { return _definition; }
            set { _definition = value.NullSafeTrim(); }
        }
    }
}