﻿namespace Glossy.Application.Commands
{
    public class RedefineTermCommand
    {
        private string _newDefinition;
        private string _newTerm;
        private string _term;

        public RedefineTermCommand()
        {
        }

        public RedefineTermCommand(string term, string definiton)
        {
            Term = term;
            NewTerm = term;
            NewDefinition = definiton;
        }

        public string Term
        {
            get { return _term; }
            set { _term = value.NullSafeTrim(); }
        }

        public string NewTerm
        {
            get { return _newTerm; }
            set { _newTerm = value.NullSafeTrim(); }
        }

        public string NewDefinition
        {
            get { return _newDefinition; }
            set { _newDefinition = value.NullSafeTrim(); }
        }
    }
}