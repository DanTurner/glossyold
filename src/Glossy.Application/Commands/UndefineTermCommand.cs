﻿namespace Glossy.Application.Commands
{
    public class UndefineTermCommand
    {
        private string _term;

        public UndefineTermCommand()
        {
        }

        public UndefineTermCommand(string term)
            : this()
        {
            Term = term;
        }

        public string Term
        {
            get { return _term; }
            set { _term = value.NullSafeTrim(); }
        }
    }
}