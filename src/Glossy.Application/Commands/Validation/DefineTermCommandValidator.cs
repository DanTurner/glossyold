﻿using System;
using FluentValidation;
using Glossy.Application.Services;
using Glossy.Core;

namespace Glossy.Application.Commands.Validation
{
    public class DefineTermCommandValidator : AbstractValidator<DefineTermCommand>
    {
        private readonly IGlossaryService _service;

        public DefineTermCommandValidator(IGlossaryService service)
        {
            _service = service;

            RuleFor(command => command.Term)
                .NotEmpty()
                .Must(BeUndefined).WithMessage("This term has already been defined");
            RuleFor(command => command.Definition)
                .NotEmpty();
        }

        public bool BeUndefined(DefineTermCommand command, string term)
        {
            if (String.IsNullOrEmpty(term) || String.IsNullOrEmpty(command.Definition))
            {
                return true;
            }

            Glossary glossary = _service.Get();

            return !glossary.IsDefined(term);
        }
    }
}