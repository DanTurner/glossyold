﻿using System;
using FluentValidation;
using Glossy.Application.Services;
using Glossy.Core;

namespace Glossy.Application.Commands.Validation
{
    public class RedefineTermCommandValidator : AbstractValidator<RedefineTermCommand>
    {
        private readonly IGlossaryService _service;

        public RedefineTermCommandValidator(IGlossaryService service)
        {
            _service = service;

            //command.Term is not user input and therefore does not need to be validated

            RuleFor(x => x.NewTerm)
                .NotEmpty()
                .Must(BeUndefined).WithMessage("This term has already been defined");

            RuleFor(x => x.NewDefinition)
                .NotEmpty();
        }

        public bool BeUndefined(RedefineTermCommand command, string newTerm)
        {
            if (String.IsNullOrEmpty(newTerm) || String.IsNullOrEmpty(command.NewDefinition) ||
                command.Term.Equals(newTerm))
            {
                return true;
            }

            Glossary glossary = _service.Get();

            return !glossary.IsDefined(newTerm);
        }
    }
}