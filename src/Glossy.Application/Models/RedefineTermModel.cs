﻿using Glossy.Application.Commands;

namespace Glossy.Application.Models
{
    public class RedefineTermModel
    {
        public RedefineTermModel(string term, string definition)
        {
            Command = new RedefineTermCommand(term, definition);
        }

        public RedefineTermCommand Command { get; set; }
    }
}