﻿using System.Collections.Generic;
using System.Linq;

namespace Glossy.Application.Models
{
    public class GlossaryModel
    {
        private readonly List<Entry> _entries;

        public GlossaryModel(IEnumerable<Entry> entries)
        {
            _entries = entries.OrderBy(entry => entry.Term).ToList();
        }

        public IList<Entry> Entries
        {
            get { return _entries.AsReadOnly(); }
        }

        #region Nested type: Entry

        public class Entry
        {
            private readonly string _definition;
            private readonly string _term;

            public Entry(string term, string definition)
            {
                _term = term;
                _definition = definition;
            }

            public string Term
            {
                get { return _term; }
            }

            public string Definition
            {
                get { return _definition; }
            }
        }

        #endregion
    }
}