﻿using Glossy.Application.Commands;

namespace Glossy.Application.Models
{
    public class DefineTermModel
    {
        public DefineTermModel()
        {
            Command = new DefineTermCommand();
        }

        public DefineTermCommand Command { get; set; }
    }
}