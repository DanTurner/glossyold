﻿using Glossy.Application.Services;
using Glossy.Core;
using Glossy.Application.Exceptions;

namespace Glossy.Application.Models.Factories
{
    public class RedefineTermModelFactory : IModelFactory<RedefineTermQuery, RedefineTermModel>
    {
        private readonly IGlossaryService _service;

        public RedefineTermModelFactory(IGlossaryService service)
        {
            _service = service;
        }

        #region IModelFactory<RedefineTermQuery,RedefineTermModel> Members

        public RedefineTermModel Create(RedefineTermQuery input)
        {
            Glossary glossary = _service.Get();

            var term = input.Term;

            if (!glossary.IsDefined(term))
                throw new NotFoundException("Term not found");

            Definition defintion = glossary.Lookup(input.Term);

            return new RedefineTermModel(input.Term, defintion);
        }

        #endregion
    }
}