﻿using System.Collections.Generic;
using Glossy.Application.Services;
using Glossy.Core;

namespace Glossy.Application.Models.Factories
{
    public class GlossaryModelFactory : IModelFactory<GlossaryModel>
    {
        private readonly IGlossaryService _service;

        public GlossaryModelFactory(IGlossaryService service)
        {
            _service = service;
        }

        #region IModelFactory<GlossaryModel> Members

        public GlossaryModel Create()
        {
            Glossary glossary = _service.Get();

            var entries = new List<GlossaryModel.Entry>();

            foreach (Term term in glossary.Terms())
            {
                Definition definition = glossary.Lookup(term);
                entries.Add(new GlossaryModel.Entry(term.Value, definition.Value));
            }

            return new GlossaryModel(entries);
        }

        #endregion
    }
}