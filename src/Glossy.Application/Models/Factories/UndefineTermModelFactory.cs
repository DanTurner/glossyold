﻿using Glossy.Application.Services;
using Glossy.Core;
using Glossy.Application.Exceptions;

namespace Glossy.Application.Models.Factories
{
    public class UndefineTermModelFactory : IModelFactory<UndefineTermQuery, UndefineTermModel>
    {
        private readonly IGlossaryService _service;

        public UndefineTermModelFactory(IGlossaryService service)
        {
            _service = service;
        }

        #region IModelFactory<UndefineTermQuery,UndefineTermModel> Members

        public UndefineTermModel Create(UndefineTermQuery input)
        {
            Glossary glossary = _service.Get();

            var term = input.Term;

            if (!glossary.IsDefined(term))
                throw new NotFoundException("Term not found");

            Definition defintion = glossary.Lookup(input.Term);

            return new UndefineTermModel(input.Term, defintion);
        }

        #endregion
    }
}