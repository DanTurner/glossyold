﻿using Glossy.Application.Commands;

namespace Glossy.Application.Models
{
    public class UndefineTermModel
    {
        public UndefineTermModel(string term, string defintion)
        {
            Command = new UndefineTermCommand(term);
            Definition = defintion;
        }

        public UndefineTermCommand Command { get; set; }

        public string Definition { get; set; }
    }
}