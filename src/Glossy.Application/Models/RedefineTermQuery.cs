﻿namespace Glossy.Application.Models
{
    public class RedefineTermQuery
    {
        public string Term { get; set; }
    }
}