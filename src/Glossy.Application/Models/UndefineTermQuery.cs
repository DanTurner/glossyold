﻿namespace Glossy.Application.Models
{
    public class UndefineTermQuery
    {
        public string Term { get; set; }
    }
}