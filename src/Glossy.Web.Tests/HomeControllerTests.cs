﻿using System.Web.Mvc;
using Glossy.Web.Controllers;
using NUnit.Framework;

namespace Glossy.Web.Tests
{
    [TestFixture]
    public class HomeControllerTests
    {
        [Test]
        public void IndexIsViewResult()
        {
            var controller = new HomeController();

            ActionResult result = controller.Index();

            Assert.IsInstanceOf<ViewResult>(result);
        }
    }
}