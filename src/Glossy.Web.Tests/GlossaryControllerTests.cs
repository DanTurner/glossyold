﻿using System.Collections.Specialized;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Glossy.Application.Commands;
using Glossy.Application.Infrastructure;
using Glossy.Application.Models;
using Glossy.Web.Controllers;
using Moq;
using NUnit.Framework;

namespace Glossy.Web.Tests
{
    [TestFixture]
    public class GlossaryControllerTests
    {
        [Test]
        public void Define_returns_ViewResult()
        {
            var commandBus = new Mock<ICommandBus>();
            var modelFactory = new Mock<IModelBuilder>();

            var controller = new GlossaryController(commandBus.Object, modelFactory.Object);

            ActionResult result = controller.Define();

            Assert.IsInstanceOf<ViewResult>(result);
        }

        [Test]
        public void Define_uses_the_model_DefineTermModel()
        {
            var commandBus = new Mock<ICommandBus>();
            var modelFactory = new Mock<IModelBuilder>();

            var controller = new GlossaryController(commandBus.Object, modelFactory.Object);

            ActionResult result = controller.Define();

            Assert.IsInstanceOf<ViewResult>(result);

            var viewResult = (ViewResult) result;

            Assert.IsInstanceOf<DefineTermModel>(viewResult.Model);
        }

        [Test]
        public void Define_with_invalid_input_does_not_execute_command()
        {
            var command = new DefineTermCommand();
            command.Term = "";
            command.Definition = "This is a term";

            var commandBus = new Mock<ICommandBus>();
            commandBus.Setup(u => u.Execute(command));

            var modelFactory = new Mock<IModelBuilder>();

            var controller = new GlossaryController(commandBus.Object, modelFactory.Object);
            controller.ModelState.AddModelError("Term", "Term cannot be empty");

            ActionResult result = controller.Define(command);

            commandBus.Verify(u => u.Execute(command), Times.Never());
        }

        [Test]
        public void Define_with_invalid_input_returns_Define_view()
        {
            var command = new DefineTermCommand();
            command.Term = "";
            command.Definition = "This is a term";

            var commandBus = new Mock<ICommandBus>();
            commandBus.Setup(u => u.Execute(command));

            var modelFactory = new Mock<IModelBuilder>();

            var controller = new GlossaryController(commandBus.Object, modelFactory.Object);
            controller.ModelState.AddModelError("Term", "Term cannot be empty");

            ActionResult result = controller.Define(command);

            Assert.IsInstanceOf<ViewResult>(result);

            var viewResult = (ViewResult) result;

            Assert.IsInstanceOf<DefineTermModel>(viewResult.Model);

            var model = (DefineTermModel) viewResult.Model;
        }

        [Test]
        public void Define_with_valid_input_executes_command()
        {
            var command = new DefineTermCommand();
            command.Term = "Term A";
            command.Definition = "This is a term";

            var commandBus = new Mock<ICommandBus>();
            commandBus.Setup(u => u.Execute(command));

            var modelFactory = new Mock<IModelBuilder>();

            var controller = new GlossaryController(commandBus.Object, modelFactory.Object);

            ActionResult result = controller.Define(command);

            commandBus.Verify(u => u.Execute(command), Times.Once());
        }

        [Test]
        public void Define_with_valid_input_redirects_to_Index_after_execute()
        {
            var command = new DefineTermCommand();
            command.Term = "Term A";
            command.Definition = "This is a term";

            var commandBus = new Mock<ICommandBus>();
            commandBus.Setup(u => u.Execute(command));

            var modelFactory = new Mock<IModelBuilder>();

            var controller = new GlossaryController(commandBus.Object, modelFactory.Object);

            ActionResult result = controller.Define(command);

            Assert.IsInstanceOf<RedirectToRouteResult>(result);

            var redirectResult = (RedirectToRouteResult) result;

            Assert.AreEqual("Index", redirectResult.RouteValues["action"]);
        }

        [Test]
        public void Index_returns_ViewResult()
        {
            var commandBus = new Mock<ICommandBus>();
            var modelFactory = new Mock<IModelBuilder>();

            var controller = new GlossaryController(commandBus.Object, modelFactory.Object);

            ActionResult result = controller.Index();

            Assert.IsInstanceOf<ViewResult>(result);
        }

        [Test]
        public void Index_uses_the_model_GlossaryModel()
        {
            var model = new GlossaryModel(new[]
                                              {
                                                  new GlossaryModel.Entry("Term A", "Definition A"),
                                                  new GlossaryModel.Entry("Term B", "Definition B")
                                              });

            var commandBus = new Mock<ICommandBus>();
            var modelFactory = new Mock<IModelBuilder>();
            modelFactory.Setup(u => u.Create<GlossaryModel>()).Returns(model);

            var controller = new GlossaryController(commandBus.Object, modelFactory.Object);

            ActionResult result = controller.Index();

            Assert.IsInstanceOf<ViewResult>(result);

            var viewResult = (ViewResult) result;

            Assert.AreSame(model, viewResult.Model);
        }

        [Test]
        public void Redefine_returns_ViewResult()
        {
            var request = new Mock<HttpRequestBase>();

            var context = new Mock<HttpContextBase>();
            context.Setup(u => u.Request).Returns(request.Object);

            var headers = new NameValueCollection();

            request.Setup(u => u.Headers).Returns(headers);

            var model = new RedefineTermModel("Term A", "This is term A");
            var input = new RedefineTermQuery {Term = model.Command.Term};

            var commandBus = new Mock<ICommandBus>();
            var modelFactory = new Mock<IModelBuilder>();
            modelFactory.Setup(u => u.Create<RedefineTermQuery, RedefineTermModel>(input)).Returns(model);

            var controller = new GlossaryController(commandBus.Object, modelFactory.Object);
            controller.ControllerContext = new ControllerContext(new RequestContext(context.Object, new RouteData()),
                                                                 controller);

            ActionResult result = controller.Redefine(input);

            Assert.IsInstanceOf<ViewResult>(result);
        }

        [Test]
        public void Redefine_uses_the_model_RedefineTermModel()
        {
            var request = new Mock<HttpRequestBase>();

            var context = new Mock<HttpContextBase>();
            context.Setup(u => u.Request).Returns(request.Object);

            var headers = new NameValueCollection();

            request.Setup(u => u.Headers).Returns(headers);

            var model = new RedefineTermModel("Term A", "This is term A");
            var input = new RedefineTermQuery {Term = model.Command.Term};

            var commandBus = new Mock<ICommandBus>();
            var modelFactory = new Mock<IModelBuilder>();
            modelFactory.Setup(u => u.Create<RedefineTermQuery, RedefineTermModel>(input)).Returns(model);

            var controller = new GlossaryController(commandBus.Object, modelFactory.Object);
            controller.ControllerContext = new ControllerContext(new RequestContext(context.Object, new RouteData()),
                                                                 controller);

            ActionResult result = controller.Redefine(input);

            Assert.IsInstanceOf<ViewResult>(result);

            var viewResult = (ViewResult) result;

            Assert.IsInstanceOf<RedefineTermModel>(viewResult.Model);
        }

        [Test]
        public void Redefine_with_invalid_input_does_not_execute_command()
        {
            var model = new RedefineTermModel("Term A", "This is term A");
            var input = new RedefineTermQuery {Term = model.Command.Term};

            var commandBus = new Mock<ICommandBus>();
            var modelFactory = new Mock<IModelBuilder>();
            modelFactory.Setup(u => u.Create<RedefineTermQuery, RedefineTermModel>(input)).Returns(model);

            var controller = new GlossaryController(commandBus.Object, modelFactory.Object);
            controller.ModelState.AddModelError("NewTerm", "Invalid term");

            var command = new RedefineTermCommand(model.Command.Term, model.Command.NewDefinition);
            command.NewTerm = "";
            command.NewDefinition = "This is a new term";

            ActionResult result = controller.Redefine(command);

            commandBus.Verify(u => u.Execute(command), Times.Never());
        }

        [Test]
        public void Redefine_with_invalid_input_returns_Redefine_view()
        {
            var model = new RedefineTermModel("Term A", "This is term A");
            var input = new RedefineTermQuery {Term = model.Command.Term};

            var commandBus = new Mock<ICommandBus>();
            var modelFactory = new Mock<IModelBuilder>();

            modelFactory.Setup(u => u.Create<RedefineTermQuery, RedefineTermModel>(
                It.Is<RedefineTermQuery>(i => i.Term == input.Term)
                                        )).Returns(model);

            var controller = new GlossaryController(commandBus.Object, modelFactory.Object);
            controller.ModelState.AddModelError("NewTerm", "Invalid term");

            var command = new RedefineTermCommand(model.Command.Term, model.Command.NewDefinition);
            command.NewTerm = "";
            command.NewDefinition = "This is a new term";

            ActionResult result = controller.Redefine(command);

            Assert.IsInstanceOf<ViewResult>(result);

            var viewResult = (ViewResult) result;

            Assert.IsInstanceOf<RedefineTermModel>(viewResult.Model);

            var returnModel = (RedefineTermModel) viewResult.Model;

            Assert.AreEqual(model, returnModel);

            modelFactory.Verify(u => u.Create<RedefineTermQuery, RedefineTermModel>(
                It.Is<RedefineTermQuery>(i => i.Term == input.Term)
                                         ), Times.Once());
        }

        [Test]
        public void Redefine_with_valid_input_executes_command()
        {
            var request = new Mock<HttpRequestBase>();

            var context = new Mock<HttpContextBase>();
            context.Setup(u => u.Request).Returns(request.Object);

            var headers = new NameValueCollection();

            request.Setup(u => u.Headers).Returns(headers);

            var model = new RedefineTermModel("Term A", "This is term A");
            var input = new RedefineTermQuery {Term = model.Command.Term};

            var commandBus = new Mock<ICommandBus>();
            var modelFactory = new Mock<IModelBuilder>();
            modelFactory.Setup(u => u.Create<RedefineTermQuery, RedefineTermModel>(input)).Returns(model);

            var controller = new GlossaryController(commandBus.Object, modelFactory.Object);
            controller.ControllerContext = new ControllerContext(new RequestContext(context.Object, new RouteData()),
                                                                 controller);

            var command = new RedefineTermCommand(model.Command.Term, model.Command.NewDefinition);
            command.NewTerm = "Term C";
            command.NewDefinition = "This is a new term";

            ActionResult result = controller.Redefine(command);

            commandBus.Verify(u => u.Execute(command), Times.Once());
        }

        [Test]
        public void Redefine_with_valid_input_redirects_to_Index_after_execute()
        {
            var request = new Mock<HttpRequestBase>();

            var context = new Mock<HttpContextBase>();
            context.Setup(u => u.Request).Returns(request.Object);

            var headers = new NameValueCollection();

            request.Setup(u => u.Headers).Returns(headers);

            var model = new RedefineTermModel("Term A", "This is term A");
            var input = new RedefineTermQuery {Term = model.Command.Term};

            var commandBus = new Mock<ICommandBus>();
            var modelFactory = new Mock<IModelBuilder>();
            modelFactory.Setup(u => u.Create<RedefineTermQuery, RedefineTermModel>(input)).Returns(model);

            var controller = new GlossaryController(commandBus.Object, modelFactory.Object);
            controller.ControllerContext = new ControllerContext(new RequestContext(context.Object, new RouteData()),
                                                                 controller);

            var command = new RedefineTermCommand(model.Command.Term, model.Command.NewDefinition);
            command.NewTerm = "";
            command.NewDefinition = "This is a new term";

            ActionResult result = controller.Redefine(command);

            Assert.IsInstanceOf<RedirectToRouteResult>(result);

            var redirectResult = (RedirectToRouteResult) result;

            Assert.AreEqual("Index", redirectResult.RouteValues["action"]);
        }

        [Test]
        public void Undefine_returns_ViewResult()
        {
            var model = new UndefineTermModel("Term A", "This is term A");
            var input = new UndefineTermQuery {Term = model.Command.Term};

            var commandBus = new Mock<ICommandBus>();
            var modelFactory = new Mock<IModelBuilder>();
            modelFactory.Setup(u => u.Create<UndefineTermQuery, UndefineTermModel>(input)).Returns(model);

            var controller = new GlossaryController(commandBus.Object, modelFactory.Object);

            ActionResult result = controller.Undefine(input);

            Assert.IsInstanceOf<ViewResult>(result);
        }

        [Test]
        public void Undefine_uses_the_model_UndefineTermModel()
        {
            var model = new UndefineTermModel("Term A", "This is term A");
            var input = new UndefineTermQuery {Term = model.Command.Term};

            var commandBus = new Mock<ICommandBus>();
            var modelFactory = new Mock<IModelBuilder>();
            modelFactory.Setup(u => u.Create<UndefineTermQuery, UndefineTermModel>(input)).Returns(model);

            var controller = new GlossaryController(commandBus.Object, modelFactory.Object);

            ActionResult result = controller.Undefine(input);

            Assert.IsInstanceOf<ViewResult>(result);

            var viewResult = (ViewResult) result;

            Assert.AreEqual(model, viewResult.Model);
        }

        [Test]
        public void Undefine_with_valid_input_executes_command()
        {
            var request = new Mock<HttpRequestBase>();

            var context = new Mock<HttpContextBase>();
            context.Setup(u => u.Request).Returns(request.Object);

            var headers = new NameValueCollection();

            request.Setup(u => u.Headers).Returns(headers);

            var command = new UndefineTermCommand();
            command.Term = "Term A";

            var commandBus = new Mock<ICommandBus>();
            commandBus.Setup(u => u.Execute(command));

            var modelFactory = new Mock<IModelBuilder>();

            var controller = new GlossaryController(commandBus.Object, modelFactory.Object);
            controller.ControllerContext = new ControllerContext(new RequestContext(context.Object, new RouteData()),
                                                                 controller);

            ActionResult result = controller.Undefine(command);


            commandBus.Verify(u => u.Execute(command), Times.Once());
        }

        [Test]
        public void Undefine_with_valid_input_redirects_to_Index_after_execute()
        {
            var request = new Mock<HttpRequestBase>();

            var context = new Mock<HttpContextBase>();
            context.Setup(u => u.Request).Returns(request.Object);

            var headers = new NameValueCollection();

            request.Setup(u => u.Headers).Returns(headers);

            var command = new UndefineTermCommand();
            command.Term = "Term A";

            var commandBus = new Mock<ICommandBus>();
            commandBus.Setup(u => u.Execute(command));

            var modelFactory = new Mock<IModelBuilder>();

            var controller = new GlossaryController(commandBus.Object, modelFactory.Object);
            controller.ControllerContext = new ControllerContext(new RequestContext(context.Object, new RouteData()),
                                                                 controller);

            ActionResult result = controller.Undefine(command);

            Assert.IsInstanceOf<RedirectToRouteResult>(result);

            var redirectResult = (RedirectToRouteResult) result;

            Assert.AreEqual("Index", redirectResult.RouteValues["action"]);
        }
    }
}