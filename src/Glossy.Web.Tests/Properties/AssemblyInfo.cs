﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Glossy.Web.Tests")]
[assembly: AssemblyDescription("These are unit tests for Glossy.Web.")]
[assembly: ComVisible(false)]
[assembly: Guid("c040ec82-ea16-4a7a-a77f-4c1f79766b96")]

// Version information in SolutionInfo.cs