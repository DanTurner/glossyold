﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;

namespace Glossy.Integration.Tests
{
    [TestFixture(Ignore=true)]
    public class IntegrationTestScript
    {
        #region Setup/Teardown

        [SetUp]
        public void SetupTest()
        {
            DefineTerm = Guid.NewGuid().ToString();
            DefineDefinition = Guid.NewGuid().ToString();

            RedefineTerm = Guid.NewGuid().ToString();
            RedefineDefinition = Guid.NewGuid().ToString();
        }

        #endregion

        public enum Browser
        {
            Chrome,
            Firefox,
            FirefoxWithoutJavascript,
            InternetExplorer
        }

        private string DefineTerm { get; set; }

        private string DefineDefinition { get; set; }

        private string RedefineTerm { get; set; }

        private string RedefineDefinition { get; set; }

        public static IWebDriver CreateWebDriver(Browser browser)
        {
            switch (browser)
            {
                case Browser.Chrome:
                    return new ChromeDriver("..\\..\\..\\..\\tools\\chromedriver");
                case Browser.Firefox:
                    return new FirefoxDriver();
                case Browser.FirefoxWithoutJavascript:
                    var profile = new FirefoxProfile();
                    profile.SetPreference("javascript.enabled", false);
                    return new FirefoxDriver(profile);
                case Browser.InternetExplorer:
                    return new InternetExplorerDriver();
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary>
        /// This is a basic integration test script that will exercise each function in the web application.
        /// Ideally in a perfect world each discrete user iteraction within this script would occur in its own test,
        /// with the data store restored to a known state before each test.
        /// </summary>
        /// <param name="browserType"></param>
        [Test, Sequential]
        public void can_view_define_redefine_and_undefine_a_term(
            [Values(Browser.Chrome, Browser.Firefox, Browser.FirefoxWithoutJavascript)] Browser browserType)
        {
            IWebDriver Selenium = CreateWebDriver(browserType);

            Selenium.Manage().Timeouts().ImplicitlyWait(new TimeSpan(0, 0, 10));

            //Load Home Page
            Selenium.Url = "http://localhost/Glossy";

            IWebElement glossaryLink = Selenium.FindElement(By.LinkText("View the Glossary"));
            glossaryLink.Click();

            //Define Term
            IWebElement defineLink = Selenium.FindElement(By.LinkText("Define a term"));
            defineLink.Click();

            IWebElement defineLegend = Selenium.FindElement(By.TagName("legend"));
            Assert.AreEqual("Define a term", defineLegend.Text);

            //Define Term - Missing Value Errors
            IWebElement defineSubmit = Selenium.FindElement(By.XPath("//input[@value='Define']"));
            defineSubmit.Click();

            ReadOnlyCollection<IWebElement> errors = Selenium.FindElements(By.ClassName("field-validation-error"));
            Assert.AreEqual(2, errors.Count);

            Assert.IsNotNull(errors.Single(x => String.Equals(x.Text, "'Term' should not be empty.")));
            Assert.IsNotNull(errors.Single(x => String.Equals(x.Text, "'Definition' should not be empty.")));

            //Define Term - Success
            Selenium.FindElement(By.Name("Command.Term")).SendKeys(DefineTerm);
            Selenium.FindElement(By.Name("Command.Definition")).SendKeys(DefineDefinition);
            Selenium.FindElement(By.XPath("//input[@value='Define']")).Click();

            Selenium.FindElement(By.XPath(String.Format("//h3[text()='{0}']", DefineTerm)));
            Selenium.FindElement(By.XPath(String.Format("//p[text()='{0}']", DefineDefinition)));

            //Redefine Term - Missing Value Errors
            Selenium.FindElement(By.CssSelector(String.Format("a[href$='Redefine?Term={0}']", DefineTerm))).Click();

            Thread.Sleep(200); //There is a issue in the ChromeDriver, it sometimes doesn't work without this

            IWebElement redefineLegend = Selenium.FindElement(By.TagName("legend"));
            Assert.AreEqual("Redefine a term", redefineLegend.Text);

            Assert.AreEqual(DefineTerm, Selenium.FindElement(By.Name("Command.Term")).GetAttribute("value"));
            //Hidden containing current "Term"
            Selenium.FindElement(By.Name("Command.NewTerm")).Clear();
            Selenium.FindElement(By.Name("Command.NewDefinition")).Clear();
            Selenium.FindElement(By.XPath("//input[@value='Redefine']")).Click();

            errors = Selenium.FindElements(By.ClassName("field-validation-error"));
            Assert.AreEqual(2, errors.Count);

            Assert.IsNotNull(errors.Single(x => String.Equals(x.Text, "'New Term' should not be empty.")));
            Assert.IsNotNull(errors.Single(x => String.Equals(x.Text, "'New Definition' should not be empty.")));

            //Redefine Term - Success
            Assert.AreEqual(DefineTerm, Selenium.FindElement(By.Name("Command.Term")).GetAttribute("value"));
            //Hidden containing current "Term"
            IWebElement newTerm = Selenium.FindElement(By.Name("Command.NewTerm"));
            newTerm.Clear();
            newTerm.SendKeys(RedefineTerm);
            IWebElement newDef = Selenium.FindElement(By.Name("Command.NewDefinition"));
            newDef.Clear();
            newDef.SendKeys(RedefineDefinition);
            Selenium.FindElement(By.XPath("//input[@value='Redefine']")).Click();

            Selenium.FindElement(By.XPath(String.Format("//h3[text()='{0}']", RedefineTerm)));
            Selenium.FindElement(By.XPath(String.Format("//p[text()='{0}']", RedefineDefinition)));

            //Undefine - Success
            Selenium.FindElement(By.CssSelector(String.Format("a[href$='Undefine?Term={0}']", RedefineTerm))).Click();

            Thread.Sleep(200); //There is a issue in the ChromeDriver, it sometimes doesn't work without this

            IWebElement undefineLegend = Selenium.FindElement(By.TagName("legend"));
            Assert.AreEqual("Undefine a term", undefineLegend.Text);

            Selenium.FindElement(By.XPath("//input[@value='Undefine']")).Click();

            Thread.Sleep(400);

            Selenium.Manage().Timeouts().ImplicitlyWait(new TimeSpan(0, 0, 1));

            Assert.Throws<NoSuchElementException>(
                () => Selenium.FindElement(By.XPath(String.Format("//h3[text()='{0}']", RedefineTerm))));
            Assert.Throws<NoSuchElementException>(
                () => Selenium.FindElement(By.XPath(String.Format("//p[text()='{0}']", RedefineDefinition))));

            Selenium.Quit();
        }
    }
}