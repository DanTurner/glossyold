﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Glossy.Integration.Tests")]
[assembly: AssemblyDescription("These are the integration tests for the Glossy application.")]
[assembly: ComVisible(false)]
[assembly: Guid("b54868b3-bb90-4345-ad52-e9117ed9814c")]

// Version information in SolutionInfo.cs