﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Glossy.Core.Tests
{
    [TestFixture]
    public class GlossaryTests
    {
        #region Setup/Teardown

        [SetUp]
        public void CreateGlossary()
        {
            TestingTermA = new Term("Term A");
            TestingDefinitionA = new Definition("This is term A");
            TestingTermB = new Term("Term B");
            TestingDefinitionB = new Definition("This is term B");

            Glossary = new Glossary(new Dictionary<Term, Definition>
                                        {
                                            {TestingTermB, TestingDefinitionB},
                                            {TestingTermA, TestingDefinitionA}
                                        });
        }

        #endregion

        private Glossary Glossary { get; set; }
        private Term TestingTermA { get; set; }
        private Definition TestingDefinitionA { get; set; }

        private Term TestingTermB { get; set; }
        private Definition TestingDefinitionB { get; set; }

        [Test]
        public void can_call_IsDefined_on_existing_term()
        {
            var term = new Term("Term B");

            bool result = Glossary.IsDefined(term);

            Assert.IsTrue(result);
        }

        [Test]
        public void can_call_IsDefined_on_non_existing_term()
        {
            var term = new Term("Term C");

            bool result = Glossary.IsDefined(term);

            Assert.IsFalse(result);
        }

        [Test]
        public void can_create_glossary_with_initialising_ctor()
        {
            var glossary = new Glossary(new Dictionary<Term, Definition>
                                            {
                                                {TestingTermB, TestingDefinitionB},
                                                {TestingTermA, TestingDefinitionA}
                                            });

            Assert.AreEqual(TestingDefinitionA, glossary.Lookup(TestingTermA));
            Assert.AreEqual(TestingDefinitionB, glossary.Lookup(TestingTermB));
        }

        [Test]
        public void can_define_new_term()
        {
            var term = new Term("Term C");
            var definition = new Definition("This is term C");

            Glossary.Define(term, definition);

            Assert.AreEqual(definition, Glossary.Lookup(term));
            Assert.AreEqual(3, Glossary.Terms().Count());
        }

        [Test]
        public void can_enumerate_existing_terms()
        {
            IEnumerable<Term> terms = Glossary.Terms();

            Assert.AreEqual(2, terms.Count());
            Assert.AreEqual(1, terms.Count(x => x.Equals(TestingTermA)));
            Assert.AreEqual(1, terms.Count(x => x.Equals(TestingTermB)));
        }

        [Test]
        public void can_lookup_existing_term()
        {
            Definition result = Glossary.Lookup(TestingTermA);

            Assert.AreEqual(TestingDefinitionA, result);
        }

        [Test]
        public void can_undefine_existing_term()
        {
            Glossary.Undefine(TestingTermB);

            Assert.IsFalse(Glossary.IsDefined(TestingTermB));
            Assert.AreEqual(1, Glossary.Terms().Count());
        }

        [Test]
        public void cannot_define_existing_term()
        {
            Term term = TestingTermB;
            var definition = new Definition("This is an alternate term B");

            Assert.Throws<InvalidOperationException>(() => Glossary.Define(term, definition));
        }

        [Test]
        public void cannot_lookup_nonexisting_term()
        {
            var term = new Term("Term C");

            Assert.Throws<ArgumentOutOfRangeException>(() => Glossary.Lookup(term));
        }

        [Test]
        public void cannot_undefine_nonexisting_term()
        {
            var term = new Term("Term C");

            Assert.Throws<ArgumentOutOfRangeException>(() => Glossary.Undefine(term));
        }
    }
}