﻿using System;
using NUnit.Framework;

namespace Glossy.Core.Tests
{
    [TestFixture]
    public class TermTests
    {
        [Test]
        public void can_create_term_with_ctor()
        {
            var term = new Term("Term A");

            Assert.AreEqual("Term A", term.Value);
        }

        [Test]
        public void cannot_create_term_with_empty_value()
        {
            Assert.Throws<ArgumentNullException>(() => new Term("  "));
        }

        [Test]
        public void defintion_value_is_trimmed()
        {
            var term = new Term("   Term A  ");

            Assert.AreEqual("Term A", term.Value);
        }

        [Test]
        public void equals_with_another_instance_of_same_value_is_true()
        {
            var term = new Term("Term A");
            var other = new Term("Term A");

            Assert.IsTrue(term.Equals(other));
        }

        [Test]
        public void equals_with_another_value_is_false()
        {
            var term = new Term("Term A");
            var other = new Term("Term B");

            Assert.IsFalse(term.Equals(other));
        }

        [Test]
        public void equals_with_null_is_false()
        {
            var term = new Term("Term A");

            Assert.IsFalse(term.Equals(null));
        }

        [Test]
        public void hashcode_equals_Value_hashcode()
        {
            var term = new Term("Term A");

            Assert.AreEqual(term.Value.GetHashCode(), term.GetHashCode());
        }

        [Test]
        public void string_to_term_implicit_operator_returns_null_for_whitespace()
        {
            Term term = "         ";

            Assert.IsNull(term);
        }

        [Test]
        public void string_to_term_implicit_operator_with_non_empty_string_returns_term()
        {
            Term term = "Term A";

            Assert.AreEqual("Term A", term.Value);
        }

        [Test]
        public void term_ToString_returns_Value()
        {
            var term = new Term("Term A");

            string output = term.ToString();

            Assert.AreEqual(term.Value, output);
        }

        [Test]
        public void term_to_string_implicit_operator_returns_Value()
        {
            var term = new Term("Term A");

            string result = term;

            Assert.AreEqual(term.Value, result);
        }
    }
}