﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Glossy.Core.Tests")]
[assembly: AssemblyDescription("These are unit tests for Glossy.Core.")]
[assembly: ComVisible(false)]
[assembly: Guid("b6bd656e-e6ac-4a63-b728-2223f07c2f77")]

// Version information in SolutionInfo.cs