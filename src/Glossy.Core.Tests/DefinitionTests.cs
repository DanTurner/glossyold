﻿using System;
using NUnit.Framework;

namespace Glossy.Core.Tests
{
    [TestFixture]
    public class DefinitionTests
    {
        [Test]
        public void can_create_definition_with_ctor()
        {
            var definition = new Definition("Definition A");

            Assert.AreEqual("Definition A", definition.Value);
        }

        [Test]
        public void cannot_create_definition_with_empty_value()
        {
            Assert.Throws<ArgumentNullException>(() => new Definition("  "));
        }

        [Test]
        public void definition_ToString_returns_Value()
        {
            var definition = new Definition("Definition A");

            string output = definition.ToString();

            Assert.AreEqual(definition.Value, output);
        }

        [Test]
        public void definition_to_string_implicit_operator_returns_Value()
        {
            var definition = new Definition("Definition A");

            string result = definition;

            Assert.AreEqual(definition.Value, result);
        }

        [Test]
        public void defintion_value_is_trimmed()
        {
            var definition = new Definition("   Definition A  ");

            Assert.AreEqual("Definition A", definition.Value);
        }

        [Test]
        public void equals_with_another_instance_of_same_value_is_true()
        {
            var definition = new Definition("Definition A");
            var other = new Definition("Definition A");

            Assert.IsTrue(definition.Equals(other));
        }

        [Test]
        public void equals_with_another_value_is_false()
        {
            var definition = new Definition("Definition A");
            var other = new Definition("Definition B");

            Assert.IsFalse(definition.Equals(other));
        }

        [Test]
        public void equals_with_null_is_false()
        {
            var definition = new Definition("Definition A");

            Assert.IsFalse(definition.Equals(null));
        }

        [Test]
        public void hashcode_equals_Value_hashcode()
        {
            var definition = new Definition("Definition A");

            Assert.AreEqual(definition.Value.GetHashCode(), definition.GetHashCode());
        }

        [Test]
        public void string_to_definition_implicit_operator_returns_null_for_whitespace()
        {
            Definition definition = "         ";

            Assert.IsNull(definition);
        }

        [Test]
        public void string_to_definition_implicit_operator_with_non_empty_string_returns_definition()
        {
            Definition definition = "Definition A";

            Assert.AreEqual("Definition A", definition.Value);
        }
    }
}