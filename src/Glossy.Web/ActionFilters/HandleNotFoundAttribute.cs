﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Glossy.Application.Exceptions;

namespace Glossy.Web.ActionFilters
{
    public class HandleNotFoundAttribute : ActionFilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            if (filterContext.Exception is NotFoundException)
            {
                filterContext.Result = new HttpNotFoundResult();
                filterContext.ExceptionHandled = true;
            }
        }
    }
}