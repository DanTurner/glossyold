﻿using System;
using System.Linq;
using System.Web.Mvc;
using Glossy.Web.Extensions;

namespace Glossy.Web.ActionFilters
{
    public class AjaxByConventionAttribute : ActionFilterAttribute
    {
        private readonly string _partialViewPrefix;

        public AjaxByConventionAttribute()
        {
            _partialViewPrefix = "_";
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
            {
                if (filterContext.Result is ViewResult)
                {
                    HandleViewResult(filterContext);
                }
                else if (filterContext.Result is RedirectToRouteResult)
                {
                    HandleRedirectResult(filterContext);
                }
            }
        }

        private void HandleRedirectResult(ActionExecutedContext filterContext)
        {
            var redirectResult = (RedirectToRouteResult) filterContext.Result;

            if (filterContext.RequestContext.HttpContext.Request.IsJsonRequest())
            {
                filterContext.Result = new JsonResult
                                           {
                                               Data = new
                                                          {
                                                              redirectResult.RouteName,
                                                              RouteValues =
                                                   redirectResult.RouteValues.Select(x => new {x.Key, x.Value})
                                                          },
                                               JsonRequestBehavior = JsonRequestBehavior.AllowGet
                                           };
            }
            else
            {
                filterContext.Result = new EmptyResult();
                ;
                filterContext.HttpContext.Response.ContentType = "text/plain";
            }
        }

        private void HandleViewResult(ActionExecutedContext filterContext)
        {
            var view = (ViewResult) filterContext.Result;

            if (filterContext.RequestContext.HttpContext.Request.IsJsonRequest())
            {
                filterContext.Result = new JsonResult
                                           {Data = view.Model, JsonRequestBehavior = JsonRequestBehavior.AllowGet};
            }
            else
            {
                string viewName = String.IsNullOrEmpty(view.ViewName)
                                      ? filterContext.ActionDescriptor.ActionName
                                      : view.ViewName;
                filterContext.Result = new PartialViewResult
                                           {
                                               TempData = view.TempData,
                                               View = view.View,
                                               ViewData = view.ViewData,
                                               ViewEngineCollection = view.ViewEngineCollection,
                                               ViewName = _partialViewPrefix + viewName
                                           };
            }
        }
    }
}