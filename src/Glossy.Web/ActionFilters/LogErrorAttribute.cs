﻿using System.Web.Mvc;
using Elmah;
using Glossy.Application.Exceptions;

namespace Glossy.Web.ActionFilters
{
    public class LogErrorAttribute : ActionFilterAttribute, IExceptionFilter
    {
        #region IExceptionFilter Members

        public void OnException(ExceptionContext filterContext)
        {
            // Log only handled exceptions, because all other will be caught by ELMAH anyway.
            if (filterContext.ExceptionHandled && !(filterContext.Exception is NotFoundException))
            {
                ErrorSignal.FromCurrentContext().Raise(filterContext.Exception);
            }
        }

        #endregion
    }
}