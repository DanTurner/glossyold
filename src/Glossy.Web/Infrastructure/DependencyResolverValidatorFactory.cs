﻿using System;
using System.Web.Mvc;
using FluentValidation;

namespace Glossy.Web.Infrastructure
{
    public class DependencyResolverValidatorFactory : IValidatorFactory
    {
        private readonly IDependencyResolver _container;

        public DependencyResolverValidatorFactory(IDependencyResolver container)
        {
            _container = container;
        }

        #region IValidatorFactory Members

        public IValidator GetValidator(Type type)
        {
            Type genericType = typeof (IValidator<>).MakeGenericType(type);

            return (IValidator) _container.GetService(genericType);
        }

        public IValidator<T> GetValidator<T>()
        {
            return _container.GetService<IValidator<T>>();
        }

        #endregion
    }
}