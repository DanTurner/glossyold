﻿using Autofac;
using Autofac.Integration.Mvc;
using FluentValidation;
using Glossy.Application.Commands.Handlers;
using Glossy.Application.Commands.Validation;
using Glossy.Application.Infrastructure;
using Glossy.Application.Infrastructure.Autofac;
using Glossy.Application.Infrastructure.RavenDB;
using Glossy.Application.Models.Factories;
using Glossy.Application.Services;
using Glossy.Web.Config;
using Raven.Client;

namespace Glossy.Web.Infrastructure
{
    public class WebModule : Module
    {
        private readonly AppConfig _config;

        public WebModule(AppConfig config)
        {
            _config = config;
        }

        protected override void Load(ContainerBuilder builder)
        {
            IDocumentStore documentStore = DocumentStoreFactory.Create(_config.RunRavenAsEmbedded);
            builder.RegisterInstance(documentStore).As<IDocumentStore>().SingleInstance();

            builder.RegisterControllers(typeof (MvcApplication).Assembly);

            builder.RegisterType<AutofacCommandBus>().As<ICommandBus>().InstancePerHttpRequest();
            builder.RegisterType<AutofacModelBuilder>().As<IModelBuilder>().InstancePerHttpRequest();

            builder.RegisterType<RavenUnitOfWork>().AsSelf().InstancePerHttpRequest();

            // This binding allows stuff like the model factories to leverage the power
            // of the underlying Raven Client API without having to worry about my UoW stuff
            // (which exists primarily for the benefit of the write model).
            // NOTE: However SaveChanges() should never be called directly on the session,
            //       only RavenUnitOfWork is entitled to do this on behalf of the CommandBus.
            builder.Register(cfg => cfg.Resolve<RavenUnitOfWork>().Session).As<IDocumentSession>().
                InstancePerHttpRequest();

            // This binding allows the CommandBus to be totally ignorant of the underlying
            // persistence mechanism.
            builder.Register(cfg => cfg.Resolve<RavenUnitOfWork>()).As<IUnitOfWork>().InstancePerHttpRequest();

            builder.RegisterType<RavenGlossaryService>().As<IGlossaryService>().InstancePerHttpRequest();

            builder.RegisterAssemblyTypes(typeof (ICommandHandler<>).Assembly)
                .Where(t => t.IsClosedTypeOf(typeof (ICommandHandler<>)))
                .AsClosedTypesOf(typeof (ICommandHandler<>)).InstancePerHttpRequest();

            builder.RegisterAssemblyTypes(typeof (IModelFactory<,>).Assembly)
                .Where(t => t.IsClosedTypeOf(typeof (IModelFactory<,>)))
                .AsClosedTypesOf(typeof (IModelFactory<,>)).InstancePerHttpRequest();

            builder.RegisterAssemblyTypes(typeof (IModelFactory<>).Assembly)
                .Where(t => t.IsClosedTypeOf(typeof (IModelFactory<>)))
                .AsClosedTypesOf(typeof (IModelFactory<>)).InstancePerHttpRequest();

            builder.RegisterAssemblyTypes(typeof (DefineTermCommandValidator).Assembly)
                .Where(t => t.IsClosedTypeOf(typeof (IValidator<>)))
                .AsClosedTypesOf(typeof (IValidator<>)).InstancePerHttpRequest();
        }
    }
}