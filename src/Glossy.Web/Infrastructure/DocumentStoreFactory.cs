﻿using Raven.Client;
using Raven.Client.Document;
using Raven.Client.Embedded;

namespace Glossy.Web.Infrastructure
{
    public class DocumentStoreFactory
    {
        public const string CONNECTION_STRING_NAME = "RavenDB";

        public static IDocumentStore Create(bool runAsEmbedded)
        {
            IDocumentStore store;
            if (runAsEmbedded)
            {
                store = new EmbeddableDocumentStore {DataDirectory = "~/App_Data/RavenDB"};
            }
            else
            {
                store = new DocumentStore {ConnectionStringName = CONNECTION_STRING_NAME};
            }
            store.Initialize();
            return store;
        }
    }
}