﻿using System;
using System.Linq;
using System.Web;

namespace Glossy.Web.Extensions
{
    public static class HttpRequestExtensions
    {
        public static bool IsJsonRequest(this HttpRequestBase request)
        {
            return
                request.AcceptTypes.Any(x => x.Equals("application/json", StringComparison.InvariantCultureIgnoreCase));
        }
    }
}