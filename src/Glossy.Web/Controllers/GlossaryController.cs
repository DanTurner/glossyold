﻿using System.Web.Mvc;
using Glossy.Application.Commands;
using Glossy.Application.Infrastructure;
using Glossy.Application.Models;
using Glossy.Web.ActionFilters;

namespace Glossy.Web.Controllers
{
    [AjaxByConvention]
    public class GlossaryController : Controller
    {
        private readonly ICommandBus _commandBus;
        private readonly IModelBuilder _modelBuilder;

        public GlossaryController(ICommandBus commandBus, IModelBuilder modelBuilder)
        {
            _commandBus = commandBus;
            _modelBuilder = modelBuilder;
        }

        [NoCache] //IE is an aggressive cacher, without this applied it would not fetch
        //the glossary after adding a term.
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index()
        {
            var model = _modelBuilder.Create<GlossaryModel>();

            return View(model);
        }

        #region Define

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Define()
        {
            var model = new DefineTermModel();

            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Define(DefineTermCommand command)
        {
            if (ModelState.IsValid)
            {
                _commandBus.Execute(command);

                return RedirectToAction("Index");
            }

            var model = new DefineTermModel();

            return View(model);
        }

        #endregion

        #region Re-Define

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Redefine(RedefineTermQuery input)
        {
            RedefineTermModel model = _modelBuilder.Create<RedefineTermQuery, RedefineTermModel>(input);

            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Redefine(RedefineTermCommand command)
        {
            if (ModelState.IsValid)
            {
                _commandBus.Execute(command);

                return RedirectToAction("Index");
            }

            var query = new RedefineTermQuery {Term = command.Term};
            RedefineTermModel model = _modelBuilder.Create<RedefineTermQuery, RedefineTermModel>(query);

            return View(model);
        }

        #endregion

        #region Undefine

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Undefine(UndefineTermQuery input)
        {
            UndefineTermModel model = _modelBuilder.Create<UndefineTermQuery, UndefineTermModel>(input);

            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Undefine(UndefineTermCommand command)
        {
            _commandBus.Execute(command);

            return RedirectToAction("Index");
        }

        #endregion
    }
}