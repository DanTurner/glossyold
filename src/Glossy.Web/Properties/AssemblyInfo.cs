﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Glossy.Web")]
[assembly:
    AssemblyDescription(
        "This is the presentation component of Glossy. It contains the views which control how it looks and the controllers which control the navigation flow."
        )]
[assembly: ComVisible(false)]
[assembly: Guid("334aad54-c690-4b25-9eb5-6e7cd3ee664a")]

// Version information in SolutionInfo.cs