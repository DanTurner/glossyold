﻿(function ($) {
    var $modal;
    var $modalBody;
    var $glossaryView;

    $(function () {
        $modal = $('<div class="modal"></div>').hide();
        $modalBody = $('<div class="modal-body"></div>');
        $glossaryView = $('.glossary-view');

        $modal.append($modalBody);
        $('body').append($modal);
        $modal.modal({ show: false });

        $('.action').live('click', function () {
            var action = $(this).attr('href');

            $modalBody.load(action, onModalLoaded);

            return false;
        });

        $(document).ajaxError(function (e, jqxhr, settings, exception) {
            $modal.modal('hide');
            var $response = $(jqxhr.responseText);
            var $content = $response.find('.content');

            if ($content.length > 0) {   //RemoteErrors: On
                $('.content').replaceWith($content);
            }
            else {   //RemoteErrors: Off
                $('html').html(jqxhr.responseText);
            }
        });
    });

    function onModalLoaded(response, status, xhr) {
        if (status !== "error") {
            $modal.modal('show');
            var $form = $modalBody.find('form');
            var $cancelButton = $modalBody.find('.btn-cancel');
            $cancelButton.click(function () {
                $modal.modal('hide');
                return false;
            });
            $form.submit(formSubmitHandler);
        }
    }

    function formSubmitHandler() {
        var $form = $(this);
        var config = $form.data();

        var formData = $form.serialize();
        var action = $form.attr('action');

        var jqxhr = $.ajax({
            url: action,
            data: formData,
            type: "POST"
        })
        .done(function (data) {
            if (data) {
                $modalBody.html(data);
                onModalLoaded();
            }
            else {
                $modal.modal('hide');
                var config = $glossaryView.data();
                $glossaryView.load(config.reloadUri);
            }
        })
        .fail(onAjaxFail);

        return false;
    }

    function onAjaxFail() {
        $modal.modal('hide');
    }
})(jQuery);