﻿using System.Collections.Specialized;
using System.Configuration;
using Glossy.Web.Infrastructure;

namespace Glossy.Web.Config
{
    public class AppConfigReader
    {
        public static AppConfig Read(NameValueCollection appSettings,
                                     ConnectionStringSettingsCollection connectionStrings)
        {
            bool runRavenAsEmbedded = (connectionStrings[DocumentStoreFactory.CONNECTION_STRING_NAME] == null);

            return new AppConfig(runRavenAsEmbedded);
        }
    }
}