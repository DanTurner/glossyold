﻿namespace Glossy.Web.Config
{
    public class AppConfig
    {
        private readonly bool _runRavenAsEmbedded;

        public AppConfig(bool runRavenAsEmbedded)
        {
            _runRavenAsEmbedded = runRavenAsEmbedded;
        }

        public bool RunRavenAsEmbedded
        {
            get { return _runRavenAsEmbedded; }
        }
    }
}