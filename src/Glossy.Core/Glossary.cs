﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Glossy.Core
{
    [DataContract]
    public class Glossary
    {
        private readonly IDictionary<Term, Definition> _entries;

        public Glossary(IDictionary<Term, Definition> entries)
        {
            _entries = entries;
        }

        [DataMember]
        private IDictionary<Term, Definition> Entries
        {
            get { return _entries; }
        }

        public IEnumerable<Term> Terms()
        {
            return Entries.Keys;
        }

        public bool IsDefined(Term term)
        {
            if (term == null)
            {
                return false;
            }
            return Entries.ContainsKey(term);
        }

        public void Define(Term term, Definition definition)
        {
            if (term == null)
            {
                throw new ArgumentNullException("term");
            }
            if (definition == null)
            {
                throw new ArgumentNullException("definition");
            }
            if (IsDefined(term))
            {
                throw new InvalidOperationException(String.Format("The term '{0}' is already defined", term));
            }
            Entries.Add(term, definition);
        }

        public void Undefine(Term term)
        {
            if (term == null)
            {
                throw new ArgumentNullException("term");
            }
            var definition = Lookup(term);
            Entries.Remove(term);
        }

        public Definition Lookup(Term term)
        {
            if (!IsDefined(term))
            {
                throw new ArgumentOutOfRangeException("term");
            }
            return Entries[term];
        }
    }
}