﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Glossy.Core")]
[assembly: AssemblyDescription("This is the core component of glossy that encapsulates the domain model.")]
[assembly: ComVisible(false)]
[assembly: Guid("035a926f-5d84-4005-b4e0-3729fb39593e")]

// Version information in SolutionInfo.cs