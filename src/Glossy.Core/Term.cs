﻿using System;

namespace Glossy.Core
{
    public class Term
    {
        private readonly string _value;

        public Term(string value)
        {
            if (String.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentNullException("value");
            }
            _value = value.Trim();
        }

        public string Value
        {
            get { return _value; }
        }

        public static implicit operator Term(string value)
        {
            return !String.IsNullOrWhiteSpace(value) ? new Term(value) : null;
        }

        public static implicit operator string(Term term)
        {
            return (term != null) ? term.Value : null;
        }

        public override string ToString()
        {
            return _value;
        }

        public override bool Equals(object obj)
        {
            var other = obj as Term;

            return (other != null && other.Value.Equals(Value));
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }
    }
}