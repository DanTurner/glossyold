﻿using System;

namespace Glossy.Core
{
    public class Definition
    {
        private readonly string _value;

        public Definition(string value)
        {
            if (String.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentNullException("value");
            }
            _value = value.Trim();
        }

        public string Value
        {
            get { return _value; }
        }

        public static implicit operator Definition(string value)
        {
            return !String.IsNullOrWhiteSpace(value) ? new Definition(value) : null;
        }

        public static implicit operator string(Definition definition)
        {
            return (definition != null) ? definition.Value : null;
        }

        public override string ToString()
        {
            return _value;
        }

        public override bool Equals(object obj)
        {
            var other = obj as Definition;

            return (other != null && other.Value.Equals(Value));
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }
    }
}